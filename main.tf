terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.64.0"
    }
  }
}

provider "azurerm" {
  # Configuration options
  features {}
}

resource "azurerm_resource_group" "vnet-rg" {
  location = var.location
  name = "${var.app_name}-vnet-${var.location}"
}

resource "azurerm_virtual_network" "vnet" {
  resource_group_name = azurerm_resource_group.vnet-rg.name
  location = var.location
  name = "${var.app_name}-vnet-${var.location}"
  address_space = ["${var.network_address}"]

  subnet {
    name = "default"
    address_prefix = "${var.network_address}"
  }
}