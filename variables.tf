variable "location" {
  type = string
  description = "Azure region to deploy in."
  default = "uksouth"

  validation {
      condition = contains(["uksouth","ukwest"], var.location)
      error_message = "Valid locations are uksouth and ukwest."
  }
}

variable "app_name" {
  type = string
  description = "Application name (used as the basis of resource names)."
}

variable "network_address" {
  type = string
  description = "Network address allocated to the application (e.g. 10.105.0.0/24)."
}